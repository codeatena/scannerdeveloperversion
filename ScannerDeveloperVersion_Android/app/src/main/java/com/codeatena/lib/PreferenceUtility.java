package com.codeatena.lib;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.example.michealsadleir.scannerdevelopervesion.App;

public class PreferenceUtility {

	public static PreferenceUtility instance = null;

	public final String PREF_NAME = "scannerdev_pref";
	public SharedPreferences sharedPreferences;
	public Editor prefEditor;

	public static PreferenceUtility getInstance() {

		if (instance == null) {
			instance = new PreferenceUtility();
		}

		return instance;
	}

	public PreferenceUtility() {
		sharedPreferences = App.getInstance().getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
		prefEditor = sharedPreferences.edit();
	}
}