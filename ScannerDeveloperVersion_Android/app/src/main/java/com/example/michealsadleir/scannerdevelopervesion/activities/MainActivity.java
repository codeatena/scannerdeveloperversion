package com.example.michealsadleir.scannerdevelopervesion.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.codeatena.lib.FragmentUtility;
import com.codeatena.lib.KeyboardUtility;
import com.example.michealsadleir.scannerdevelopervesion.adapters.OrderListAdapter;
import com.example.michealsadleir.scannerdevelopervesion.asynctasks.ParseOrderList;
import com.example.michealsadleir.scannerdevelopervesion.R;
import com.example.michealsadleir.scannerdevelopervesion.fragments.OrderListFragment;
import com.example.michealsadleir.scannerdevelopervesion.fragments.SettingsFragment;
import com.example.michealsadleir.scannerdevelopervesion.model.Order;
import com.navdrawer.SimpleSideDrawer;

import java.util.ArrayList;

public class MainActivity extends Activity {

    private String result;
    ProgressBar pBar;
    static Context appContext;
    OrderListAdapter adapter;
    ImageView imvMenu;
    SimpleSideDrawer mSlidingMenu;

    TextView txtMenuItemList;
    TextView txtMenuItemSettings;

    OrderListFragment orderListFragment;
    SettingsFragment settingsFragment;

    public static MainActivity instance;

    public static MainActivity getInstance() {
        return instance;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        appContext = getApplicationContext();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ArrayList<String> items=new ArrayList<String>();
        instance = this;

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        mSlidingMenu = new SimpleSideDrawer( this );
        mSlidingMenu.setLeftBehindContentView( R.layout.left_menu );

        imvMenu = (ImageView) findViewById(R.id.menu_imageView);

        orderListFragment = (OrderListFragment) getFragmentManager().findFragmentById(R.id.order_list_fragment);
        settingsFragment = (SettingsFragment) getFragmentManager().findFragmentById(R.id.settings_fragment);

        txtMenuItemList = (TextView) findViewById(R.id.menuitem_list_textView);
        txtMenuItemSettings = (TextView) findViewById(R.id.menuitem_settings_textView);

        FragmentUtility.getInstance().showFragment(this, orderListFragment, false);
        FragmentUtility.getInstance().hideFragment(this, settingsFragment, false);
    }

    private void initValue() {
        new ParseOrderList(null).execute(null, null, null);
        //AsyncTask<Void, Void, String> task = new DownloadDataOrderList().execute(null,null,null);
    }

    private void initEvent() {
        imvMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSlidingMenu.toggleLeftDrawer();
                KeyboardUtility.getInstance().hideKeyboard(MainActivity.this);
            }
        });
        txtMenuItemList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentUtility.getInstance().showFragment(MainActivity.this, orderListFragment, true);
                FragmentUtility.getInstance().hideFragment(MainActivity.this, settingsFragment, false);
                mSlidingMenu.toggleLeftDrawer();
            }
        });
        txtMenuItemSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentUtility.getInstance().showFragment(MainActivity.this, settingsFragment, true);
                FragmentUtility.getInstance().hideFragment(MainActivity.this, orderListFragment, false);
                mSlidingMenu.toggleLeftDrawer();
            }
        });
    }

    public void setData(ArrayList<Order> data) {
        Log.d("MainActivity", "orderList count" + data.size());
        if (OrderListFragment.getInstance() != null) {
            OrderListFragment.getInstance().setData(data);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}