package com.example.michealsadleir.scannerdevelopervesion.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

import com.example.michealsadleir.scannerdevelopervesion.asynctasks.DownloadDataOrderDetailList;
import com.example.michealsadleir.scannerdevelopervesion.adapters.OrderDetailListAdapter;
import com.example.michealsadleir.scannerdevelopervesion.R;
import com.example.michealsadleir.scannerdevelopervesion.model.Order;
import com.example.michealsadleir.scannerdevelopervesion.model.OrderDetail;

import java.util.ArrayList;

public class OrderDetailActivity extends AppCompatActivity {

    public static Order order;
    TextView txtIncrementID;
    OrderDetailListAdapter adapter;
    ListView listView;

    public static OrderDetailActivity instance;

    public static OrderDetailActivity getInstance() {
        return instance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_detail);

        instance = this;

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.order_detail_listView);
        txtIncrementID = (TextView) findViewById(R.id.increment_id_textView);

    }

    private void initValue() {
        if (order != null) {
            txtIncrementID.setText("Increment ID: " + order.getIncrement_id());
            AsyncTask<String, Void, String> task = new DownloadDataOrderDetailList(null).execute(order.getIncrement_id(),null,null);
        }
    }

    private void initEvent() {

    }

    public void setData(ArrayList<OrderDetail> data) {
        Log.e("OrderDetailActivity", "OrderListDetail count: " + data.size());

        adapter = new OrderDetailListAdapter(this, data);

        listView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // app icon in action bar clicked; goto parent activity.
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
