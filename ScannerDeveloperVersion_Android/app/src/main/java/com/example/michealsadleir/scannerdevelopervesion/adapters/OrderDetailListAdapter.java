package com.example.michealsadleir.scannerdevelopervesion.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.michealsadleir.scannerdevelopervesion.R;
import com.example.michealsadleir.scannerdevelopervesion.model.OrderDetail;

import java.util.ArrayList;

/**
 * Created by User on 11/10/2015.
 */
public class OrderDetailListAdapter extends ArrayAdapter<OrderDetail> {

    public OrderDetailListAdapter(Context context, ArrayList<OrderDetail> listItems) {
        super(context, 0, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater inflater = LayoutInflater.from(getContext());

        OrderDetail item = getItem(position);

        if (view == null) {
            view = inflater.inflate(R.layout.list_itemdetail, parent, false);
        }

        TextView txtproduct_id = (TextView) view.findViewById(R.id.product_id_textView);
        TextView txtsku = (TextView) view.findViewById(R.id.sku_textView);
        TextView txtQTY = (TextView) view.findViewById(R.id.qty_textView);
        TextView txtEAN = (TextView) view.findViewById(R.id.ean_textView);


        double QTY = item.getQty_ordered();
        String QTYString = String.valueOf(QTY);

        txtproduct_id.setText(item.getProduct_id());
        txtsku.setText(item.getSku());
        txtEAN.setText(item.getEAN());
        txtQTY.setText(QTYString);

        return view;
    }
}
