package com.example.michealsadleir.scannerdevelopervesion.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.michealsadleir.scannerdevelopervesion.R;
import com.example.michealsadleir.scannerdevelopervesion.model.Order;

import java.util.ArrayList;

/**
 * Created by User on 11/10/2015.
 */
public class OrderListAdapter extends ArrayAdapter<Order> {

    public OrderListAdapter(Context context, ArrayList<Order> listItems) {
        super(context, 0, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater inflater = LayoutInflater.from(getContext());

        Order item = getItem(position);

        if (view == null) {
            view = inflater.inflate(R.layout.list_item, parent, false);
        }

        TextView txtIncrementID = (TextView) view.findViewById(R.id.increment_id_textView);
        TextView txtCustomerID = (TextView) view.findViewById(R.id.customer_id_textView);
        TextView txtGrandTotal = (TextView) view.findViewById(R.id.grand_total_textView);

        txtIncrementID.setText(item.getIncrement_id());
        txtCustomerID.setText(item.getCustomer_id());
        txtGrandTotal.setText(item.getGrand_total());

        return view;
    }
}
