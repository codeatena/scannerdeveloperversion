package com.example.michealsadleir.scannerdevelopervesion.asynctasks;

import android.os.AsyncTask;
import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalHashtable;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.Hashtable;

/**
 * Created by michealsadleir on 26/10/2015.
 */


public class DownloadData extends AsyncTask<Void, Void, String> {
    private static final String NAMESPACE = "urn:Magento";
    private static final String URL = "http://www..com/api/v2_soap";
    public String requestDump = null;
    public String resultDump = null;

    @Override
    protected void onPreExecute() {
        super.onPreExecute();


    }


    @Override
    protected String doInBackground(Void... params) {

        try {

            SoapSerializationEnvelope env = new SoapSerializationEnvelope(SoapEnvelope.VER11);
            env.dotNet = false;
            env.xsd = SoapSerializationEnvelope.XSD;
            env.enc = SoapSerializationEnvelope.ENC;

            SoapObject request = new SoapObject(NAMESPACE, "login");

            request.addProperty("username", "");
            request.addProperty("apiKey", "");
            env.setOutputSoapObject(request);
            HttpTransportSE androidHttpTransport = new HttpTransportSE(URL);
            androidHttpTransport.debug = true;
            androidHttpTransport.call("", env);
            env.dotNet = false;
            Object result = env.getResponse();
            Log.d("sessionId", result.toString());
            String sessionId = result.toString();

            //Making a hashtable which outputs xml to pass as a filter in the request
            Hashtable<String, String> map = new Hashtable<String, String>();
            //map.put("increment_id", "100001886");
            map.put("status", "processing");
            // Add another filter by adding something like the line below
            // map.put("increment_id", "100001886");
            Log.d("hashmapValue", map.toString());

            // found out how to add an array to the soap request as magento needs a nested array
            // to be sent in ksoap2 but it doesnt support this so the work around below worked
            // with a hash table
            // https://stackoverflow.com/questions/17342327/in-android-how-to-send-complex-array-in-magento-using-ksoap2-library/25974038#25974038

            SoapObject EntityArray = new SoapObject(NAMESPACE, "shoppingCartProductEntityArray");
            EntityArray.addProperty("filter", map);


            SoapObject requestCart = new SoapObject(NAMESPACE, "salesOrderList");
            requestCart.addProperty("sessionId", sessionId);
            requestCart.addProperty("filters", EntityArray);
            (new MarshalHashtable()).register(env);

            env.setOutputSoapObject(requestCart);
            androidHttpTransport.call("", env);
            requestDump = androidHttpTransport.requestDump;
            Log.d("request", requestDump.toString());
            (new MarshalHashtable()).register(env);
            resultDump = androidHttpTransport.responseDump;
            Log.d("result", resultDump.toString());


        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultDump;
    }
    @Override
    protected void onPostExecute(String result) {
        // take the results from the soap request and pass them to the aSync parser
        AsyncTask<Void, Void, String> task = new ParseOrderList(resultDump).execute(null, null, null);
    }
}
