package com.example.michealsadleir.scannerdevelopervesion.asynctasks;

import android.os.AsyncTask;
import android.util.Log;

import com.codeatena.lib.ResourceUtility;
import com.example.michealsadleir.scannerdevelopervesion.App;
import com.example.michealsadleir.scannerdevelopervesion.activities.OrderDetailActivity;
import com.example.michealsadleir.scannerdevelopervesion.model.OrderDetail;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.util.ArrayList;

/**
 * Created by michealsadleir on 26/10/2015.
 */


public class DownloadDataOrderDetailList extends AsyncTask<String, Void, String> {
    private static final String NAMESPACE = "urn:Magento";
    private static final String URL = "";
    public String requestDump = null;
    public String resultDump = null;
    private String result;
    private ArrayList<OrderDetail> arrOrderDetails;
    ArrayList<String> items=new ArrayList<String>();
    long startTime = System.nanoTime();

    public ArrayList<OrderDetail> getOrderLists() {
        return arrOrderDetails;
    }

    public DownloadDataOrderDetailList(String result) {
        this.result = result;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        long startTime = System.nanoTime();
    }

    @Override
    protected String doInBackground(String... OrderID) {

        boolean isItemsParse = false;
        String textValue = "";
        String Orderid = OrderID[0].toString();

        ArrayList<String> items = new ArrayList<String>();

        //Download the order details

            try {
                XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
//                XmlPullParser xpp = factory.newPullParser();
//                xpp.setInput(new StringReader(this.resultDump));
                int id = ResourceUtility.getInstance().getIDFromResString("x" + Orderid, "xml");
                Log.e("DownloadOrderListDetail", "xml file resouce ID: " + id);
                XmlPullParser xpp = App.getInstance().getResources().getXml(id);

                int eventType = xpp.getEventType();
                OrderDetail orderDetail = null;
                while (eventType != XmlPullParser.END_DOCUMENT) {

                    String tagName = xpp.getName();
                    switch (eventType) {
                        case XmlPullParser.START_TAG:
                            //Log.d("Parse OrderListDetail","Starting tag for " + tagName);
                            if (tagName.equalsIgnoreCase("items")) {
                                arrOrderDetails = new ArrayList<OrderDetail>();
                                isItemsParse = true;
                            }
                            else if (tagName.equalsIgnoreCase("item")) {
                                if (isItemsParse) {
                                    orderDetail = new OrderDetail();
                                }
                            }
                            break;
                        case XmlPullParser.TEXT:
                            textValue = xpp.getText();
                            break;
                        case XmlPullParser.END_TAG:
                            if (isItemsParse) {
                                if (tagName.equalsIgnoreCase("items")) {
                                    isItemsParse = false;
                                } else if (tagName.equalsIgnoreCase("item")) {
                                    arrOrderDetails.add(orderDetail);
                                    Log.e("DownloadOrderListDetail", "order list detail inf: " + orderDetail.toString());
                                } else if (tagName.equalsIgnoreCase("product_id")) {
                                    if (textValue.isEmpty()) {
                                        orderDetail.setProduct_id("null");
                                    } else if (textValue.equals(null)) {
                                        orderDetail.setProduct_id("null");
                                    } else {
                                        orderDetail.setProduct_id(textValue);
                                    }
                                } else if (tagName.equalsIgnoreCase("sku")) {
                                    if (textValue.isEmpty()) {
                                        orderDetail.setSku("null");
                                    } else if (textValue.equals(null)) {
                                        orderDetail.setSku("null");
                                    } else {
                                        orderDetail.setSku(textValue);
                                    }
                                } else if (tagName.equalsIgnoreCase("qty_ordered")) {
                                    if (textValue.isEmpty()) {
                                        orderDetail.setQty_ordered(1);
                                    } else if (textValue.equals(null)) {
                                        orderDetail.setQty_ordered(1);
                                    } else {double myNum = 0.0000;
                                        try {
                                            myNum = Double.parseDouble(textValue.toString());
                                        } catch(NumberFormatException nfe) {
                                            System.out.println("Could not parse " + nfe);
                                        }
                                        orderDetail.setQty_ordered(myNum);
                                        orderDetail.setEAN("1234");
                                    }
                                }
                            }
                            break;

                        default:

                    }
                    eventType = xpp.next();
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;

        //Parse the results to get items

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        long stopTime = System.nanoTime();
        long elapsedTime = (stopTime - startTime);
        double seconds = (double)elapsedTime / 1000000000.0;

//        System.out.println(seconds);
//        System.out.println(orderListDetails.isEmpty());

//        for (OrderListDetail ord : orderListDetails){
//            Log.e("Product ID ", "is " + ord.getProduct_id());
//            Log.e("EAN ", "is " + ord.getEAN());
//            Log.e("QTY ", "is " + ord.getQty_ordered());
//            Log.e ("SKU " , "is " + ord.getSku());
//        }

        String d;
        if (arrOrderDetails.contains(null))
            d = "True";
        else
            d = "false";

        Log.e("Array Contains null", d.toString());

       OrderDetailActivity.getInstance().setData(arrOrderDetails);
    }
}
