package com.example.michealsadleir.scannerdevelopervesion.asynctasks;

import android.os.AsyncTask;
import android.util.Log;

import com.example.michealsadleir.scannerdevelopervesion.App;
import com.example.michealsadleir.scannerdevelopervesion.database.DatabaseHandler;
import com.example.michealsadleir.scannerdevelopervesion.R;
import com.example.michealsadleir.scannerdevelopervesion.activities.MainActivity;
import com.example.michealsadleir.scannerdevelopervesion.model.Order;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.util.ArrayList;

/**
 * Created by michealsadleir on 19/10/2015.
 */

 public class ParseOrderList extends AsyncTask<Void, Void, String> {

    private String result;
    private ArrayList<Order> arrOrders;
    DatabaseHandler db;
    ArrayList<String> items=new ArrayList<String>();


    public ParseOrderList(String result) {
        this.result = result;
        arrOrders= new ArrayList<>();
    }

    public ArrayList<Order> getOrderLists() {
        return arrOrders;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Void... params) {
        boolean status = true;
        Order currentRecord = null;
        boolean inEntry = false;
        String textValue = "";

        try {
            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            factory.setNamespaceAware(true);
//            XmlPullParser xpp = factory.newPullParser();
//            xpp.setInput(new StringReader(this.result));
            XmlPullParser xpp = App.getInstance().getResources().getXml(R.xml.xml);

            int eventType = xpp.getEventType();
            while(eventType != XmlPullParser.END_DOCUMENT){

                String tagName = xpp.getName();
                switch(eventType){
                    case XmlPullParser.START_TAG:
                        Log.d("Parse OrderList","Starting tag for " + tagName);
                        if(tagName.equalsIgnoreCase("item")){
                            inEntry = true;
                           currentRecord = new Order();
                        }
                        break;

                    case XmlPullParser.TEXT:
                        textValue = xpp.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        Log.d("Parse OrderList","Ending tag for " + tagName);
                        if(inEntry){
                            if(tagName.equalsIgnoreCase("item")){
                                arrOrders.add(currentRecord);
                                inEntry = false;
                                Log.d ("progress","this far");
                            } else if(tagName.equalsIgnoreCase("increment_id")){
                                if (textValue.isEmpty()){
                                    currentRecord.setIncrement_id("null");
                                }else if (textValue.equals(null)){
                                    currentRecord.setIncrement_id("null");
                                }
                                else {
                                    currentRecord.setIncrement_id(textValue);
                                }
                            } else if(tagName.equalsIgnoreCase("customer_id")){
                                if (textValue.isEmpty()){
                                    currentRecord.setCustomer_id("null");
                                }else if (textValue.equals(null)){
                                    currentRecord.setCustomer_id("null");
                                }
                                else {
                                    currentRecord.setCustomer_id(textValue);
                                }
                            } else if(tagName.equalsIgnoreCase("grand_total")){
                                if (textValue.isEmpty()){
                                    currentRecord.setGrand_total("null");
                                }else if (textValue.equals(null)){
                                    currentRecord.setGrand_total("null");
                                }
                                else {
                                    currentRecord.setGrand_total(textValue);
                                }
                            }
                        }
                        break;
                    default:
                        //Nothing else to do
                }
                eventType = xpp.next();
            }

            Log.d ("progress","this far2");

        } catch (Exception e){
            status = false;
            e.printStackTrace();
        }

//        for (OrderList ord : orderLists){
//            Log.d ("Customer ", "is " + ord.getCustomer_id());
//            Log.d ("Grand Total ", "is " + ord.getGrand_total());
//            Log.d ("Increment ID ", "is " + ord.getIncrement_id());
//            Log.d ("Array test" , String.valueOf(orderLists.size()));
//        }

        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        String d;
        if (arrOrders.contains(null))
            d = "True";
        else
            d = "false";

        Log.d("Array Contains null", d.toString());
        MainActivity.getInstance().setData(arrOrders);
    }
}
