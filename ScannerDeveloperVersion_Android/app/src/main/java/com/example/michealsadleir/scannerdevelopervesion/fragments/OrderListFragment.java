package com.example.michealsadleir.scannerdevelopervesion.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.app.ListFragment;
import android.view.View;
import android.widget.ListView;

import com.example.michealsadleir.scannerdevelopervesion.activities.OrderDetailActivity;
import com.example.michealsadleir.scannerdevelopervesion.adapters.OrderListAdapter;
import com.example.michealsadleir.scannerdevelopervesion.model.Order;

import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnFragmentInteractionListener}
 * interface.
 */
public class OrderListFragment extends ListFragment {

    public static OrderListFragment instance;

    public static OrderListFragment getInstance() {
        return instance;
    }

    private OnFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public OrderListFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        instance = this;
        // TODO: Change Adapter to display your content
//        setListAdapter(new ArrayAdapter<DummyContent.DummyItem>(getActivity(),
//                android.R.layout.simple_list_item_1, android.R.id.text1, DummyContent.ITEMS));
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        Order order = (Order) getListAdapter().getItem(position);
        OrderDetailActivity.order = order;
        getActivity().startActivity(new Intent(getActivity(), OrderDetailActivity.class));
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(String id);
    }

    public void setData(ArrayList<Order> data) {
        OrderListAdapter adapter = new OrderListAdapter(getActivity(), data);
        setListAdapter(adapter);
    }
}