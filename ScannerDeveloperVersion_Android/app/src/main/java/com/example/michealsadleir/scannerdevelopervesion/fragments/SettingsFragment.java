package com.example.michealsadleir.scannerdevelopervesion.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.codeatena.lib.PreferenceUtility;
import com.example.michealsadleir.scannerdevelopervesion.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsFragment extends Fragment {

    EditText edtConnection;
    EditText edtUsername;
    EditText edtPassword;

    Button btnSave;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        edtConnection = (EditText) view.findViewById(R.id.connection_editText);
        edtUsername = (EditText) view.findViewById(R.id.username_editText);
        edtPassword = (EditText) view.findViewById(R.id.password_editText);

        btnSave = (Button) view.findViewById(R.id.save_button);

        initValue();
        initEvent();

        return view;
    }

    private void initValue() {
        String connection = PreferenceUtility.getInstance().sharedPreferences.getString("connection", "");
        String username = PreferenceUtility.getInstance().sharedPreferences.getString("username", "");
        String password = PreferenceUtility.getInstance().sharedPreferences.getString("password", "");

        edtConnection.setText(connection);
        edtUsername.setText(new String(Base64.decode(username, Base64.DEFAULT )));
        edtPassword.setText(new String(Base64.decode(password, Base64.DEFAULT )));
    }

    private void initEvent() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String connection = edtConnection.getText().toString();
                String username = edtUsername.getText().toString();
                String password = edtPassword.getText().toString();

                PreferenceUtility.getInstance().prefEditor.putString("connection", connection);
                PreferenceUtility.getInstance().prefEditor.putString("username", Base64.encodeToString(username.getBytes(), Base64.DEFAULT));
                PreferenceUtility.getInstance().prefEditor.putString("password", Base64.encodeToString(password.getBytes(), Base64.DEFAULT));
                PreferenceUtility.getInstance().prefEditor.commit();
            }
        });
    }
}