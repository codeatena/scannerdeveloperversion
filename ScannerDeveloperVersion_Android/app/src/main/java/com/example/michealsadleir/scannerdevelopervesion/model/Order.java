package com.example.michealsadleir.scannerdevelopervesion.model;

/**
 * Created by michealsadleir on 19/10/2015.
 */
public class Order {
    private String increment_id;
    private String customer_id;
    private String grand_total;

    //Epmty constructor
    public Order(){
    }

    public Order(String increment_id,String customer_id,String grand_total){
        this.increment_id = increment_id;
        this.customer_id = customer_id;
        this.grand_total = grand_total;
    }

    public String getIncrement_id() {
        return increment_id;
    }

    public void setIncrement_id(String increment_id) {
        this.increment_id = increment_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getGrand_total() {
        return grand_total;
    }

    public void setGrand_total(String grand_total) {
        this.grand_total = grand_total;
    }

    @Override
    public String toString() {
        return "\n" + "Increment_id " + getIncrement_id() + "\n" +
                "Customer_id " + getCustomer_id() + "\n" +
                "Grand_total " + getGrand_total() + "\n" ;
    }
}
