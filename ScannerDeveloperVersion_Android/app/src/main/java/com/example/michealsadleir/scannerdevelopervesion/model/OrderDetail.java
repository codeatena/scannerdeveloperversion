package com.example.michealsadleir.scannerdevelopervesion.model;

/**
 * Created by michealsadleir on 20/11/2015.
 */
public class OrderDetail {
    private String product_id;
    private String sku;
    private double qty_ordered;
    private String EAN;

    public OrderDetail(){

    }

    public OrderDetail(String product_id, String sku, double qty_ordered, String EAN){
        this.product_id = product_id;
        this.sku = sku;
        this.qty_ordered = qty_ordered;
        this.EAN = EAN;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public double getQty_ordered() {
        return qty_ordered;
    }

    public void setQty_ordered(double qty_ordered) {
        this.qty_ordered = qty_ordered;
    }

    public String getEAN() {
        return EAN;
    }

    public void setEAN(String EAN) {
        this.EAN = EAN;
    }

    @Override
    public String toString() {
        return "\n" + "Sku " + getSku() + "\n" +
                "Product_id " + getProduct_id() + "\n" +
                "Qty_ordered  " + getQty_ordered() + "\n"+
                "Ean " + getEAN() + "\n"
                ;
    }
}
